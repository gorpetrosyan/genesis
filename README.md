## Project Installation

# Backend
- php version 7.3 - 7.4
- copy env.example variables to .env
- setup database configs and smtp config inside .env
- upload genesis.sql to database from project/dump
- run command "composer install".
- run command "php artisan key:generate".
- run command "php artisan storage:link".

# Frontend
 - node version 14.16
 - npm version 6.14
 - run command "npm i"
 - run command "npm run production"

