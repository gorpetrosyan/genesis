<?php
function defaultImage($img): string
{
    return asset('storage/' . str_replace('\\', '/', $img));
}
