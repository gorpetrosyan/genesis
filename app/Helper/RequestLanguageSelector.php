<?php

function getLanguagePrefixFiled(string $value, string $language): string
{
    return $value . '_' . $language;
}
