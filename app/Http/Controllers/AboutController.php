<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\AboutInterface;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    protected $aboutInterface;

    public function __construct(AboutInterface $aboutInfo)
    {
        $this->aboutInterface = $aboutInfo;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->aboutInterface->index($request->header('LANGUAGE')),200);
    }
}
