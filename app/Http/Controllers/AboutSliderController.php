<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\AboutSliderInterface;
use Illuminate\Http\Request;

class AboutSliderController extends Controller
{
    private $aboutSliderInterface;

    public function __construct(AboutSliderInterface $aboutSliderInterface)
    {
        $this->aboutSliderInterface = $aboutSliderInterface;
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->aboutSliderInterface->index(),200);
    }
}
