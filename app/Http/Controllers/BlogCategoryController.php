<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\BlogCategoryInterface;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    private $blogCategoryInterface;

    public function __construct(BlogCategoryInterface $blogCategoryInterface)
    {
        $this->blogCategoryInterface = $blogCategoryInterface;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection|null
     */
    public function index(Request $request): ?\Illuminate\Support\Collection
    {
        return $this->blogCategoryInterface->index($request->header('LANGUAGE'));
    }
}
