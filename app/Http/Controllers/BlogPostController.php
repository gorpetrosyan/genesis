<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\BlogPostInterface;
use Illuminate\Http\Request;

class BlogPostController extends Controller
{
    private $blogPost;

    public function __construct(BlogPostInterface $blogPost)
    {
        $this->blogPost = $blogPost;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $posts = $this->blogPost->index($request);
        return response()->json($posts,200);
    }

    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $post = $this->blogPost->show($id,$request->header('LANGUAGE'));
        return response()->json($post, 200);
    }
    public function indexByCategory(Request $request,$category_id): \Illuminate\Http\JsonResponse
    {
        $post = $this->blogPost->indexByCategory($request,$category_id);
        return response()->json($post, 200);
    }
}
