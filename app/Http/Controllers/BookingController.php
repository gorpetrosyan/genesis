<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Repository\Interfaces\BookingInterface;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    private $bookingInterface;

    public function __construct(BookingInterface $bookingInterface)
    {
        $this->bookingInterface = $bookingInterface;
    }

    public function create(BookingRequest $request): \Illuminate\Http\JsonResponse
    {
       $result = $this->bookingInterface->create($request->all());
       return $result ? response()->json('success',201): response()->json('failed',500);
    }
}
