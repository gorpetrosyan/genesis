<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\ContactInterface;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contactInterface;

    /**
     * ContactController constructor.
     * @param ContactInterface $contact
     */
    public function __construct(ContactInterface $contact)
    {
        $this->contactInterface = $contact;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function firstItem()
    {
        return response()->json($this->contactInterface->firstItem(),200);
    }
}
