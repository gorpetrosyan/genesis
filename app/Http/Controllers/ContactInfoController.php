<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\ContactInfoInterface;
use Illuminate\Http\Request;

class ContactInfoController extends Controller
{
    protected $contactInfoInterface;

    public function __construct(ContactInfoInterface $contactInfo)
    {
        $this->contactInfoInterface = $contactInfo;
    }


    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->contactInfoInterface->index($request->header('LANGUAGE')),200);
    }
}
