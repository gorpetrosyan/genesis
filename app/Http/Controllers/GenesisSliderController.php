<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\GenesisSliderInterface;
use Illuminate\Http\Request;

class GenesisSliderController extends Controller
{
    protected $genesisSliderInterface;

    public function __construct(GenesisSliderInterface $genesisSlider)
    {
        $this->genesisSliderInterface = $genesisSlider;
    }

    public function index()
    {
     $sliders = $this->genesisSliderInterface->index();
     return view('sliders', compact('sliders'));
    }
}
