<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\PartnerInterface;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    private $partners;

    public function __construct(PartnerInterface $partners)
    {
        $this->partners = $partners;
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        $partners = $this->partners->index();
        return response()->json($partners,200);
    }
}
