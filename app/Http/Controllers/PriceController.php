<?php

namespace App\Http\Controllers;

use App\Repository\Eloquent\PriceEloquent;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    private $priceEloquent;

    public function __construct(PriceEloquent $priceEloquent)
    {
        $this->priceEloquent = $priceEloquent;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $priceList = $this->priceEloquent->index($request);
        return response()->json($priceList,200);
    }
}
