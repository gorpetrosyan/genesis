<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\QuestionInterface;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    private $questions;

    public function __construct(QuestionInterface $questions)
    {
        $this->questions = $questions;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $questions = $this->questions->index($request->header('LANGUAGE'));
        return response()->json($questions,200);
    }
}
