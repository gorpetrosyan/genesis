<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\ServiceInterface;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    private $serviceInterface;

    public function __construct(ServiceInterface  $serviceInterface)
    {
        $this->serviceInterface = $serviceInterface;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $services = $this->serviceInterface->index($request);
        return response()->json($services,200);
    }

    public function show(Request $request, $hash): \Illuminate\Http\JsonResponse
    {
        $service = $this->serviceInterface->show($hash, $request->header('LANGUAGE'));
        return response()->json($service,200);
    }

    public function last(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->serviceInterface->last($request->header('LANGUAGE')),200);
    }
}
