<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\StatisticInterface;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    private $statistics;

    public function __construct(StatisticInterface $statistic)
    {
        $this->statistics = $statistic;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->statistics->index($request->header('LANGUAGE')),200);
    }
}
