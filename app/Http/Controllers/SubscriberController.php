<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscribeRequest;
use App\Repository\Interfaces\SubscriberInterface;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    protected $subscriberInterface;

    public function __construct(SubscriberInterface $subscriber)
    {
        $this->subscriberInterface = $subscriber;
    }

    public function subscribe(SubscribeRequest $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->subscriberInterface->subscribe($request->email, $request->header('LANGUAGE'));
        return response()->json([],$response);
    }
    public function unsubscribe($token): \Illuminate\Http\JsonResponse
    {
        $response = $this->subscriberInterface->unsubscribe($token);
        return response()->json([],$response);
    }
    public function confirm($token): \Illuminate\Http\JsonResponse
    {
        $response = $this->subscriberInterface->confirm($token);
        return response()->json([],$response);
    }
}
