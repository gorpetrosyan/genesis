<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\TeamInterface;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    protected $teamEloquent;

    public function __construct(TeamInterface $team)
    {
        $this->teamEloquent = $team;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $this->teamEloquent->index($request->header('LANGUAGE'));
        return response()->json($data,200);
    }
}
