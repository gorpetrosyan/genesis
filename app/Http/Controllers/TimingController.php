<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\TimingInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TimingController extends Controller
{
    private $TimingInterface;

    public function __construct(TimingInterface $TimingInterface)
    {
        $this->TimingInterface = $TimingInterface;
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->TimingInterface->index(),200);
    }
}
