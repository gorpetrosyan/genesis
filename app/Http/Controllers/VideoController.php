<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\VideoInterface;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    private $videoEloquent;

    public function __construct(VideoInterface $video)
    {
        $this->videoEloquent = $video;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->videoEloquent->index($request->header('LANGUAGE')),200);
    }
}
