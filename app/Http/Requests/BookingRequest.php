<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'fullName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|numeric',
            'bDay' => 'nullable|date|before:-16 years',
            'bookingDate' => 'required|date|after:start_date',
            'message' => 'nullable|string|max:500',
            'service_id' => 'required|numeric|exists:services,id',
        ];
    }
}
