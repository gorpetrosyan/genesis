<?php

namespace App\Listeners;

use App\Events\SendBlogEvent;
use App\Mail\SendBlogMail;
use App\Models\Subscriber;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class BlogSendListener
{

    private $subscribers;

    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';

    /**
     * The time (seconds) before the job should be processed.
     *
     * @var int
     */
    public $delay = 60;


    /**
     * Create the event listener.
     *
     * @param Subscriber $subscriber
     */
    public function __construct(Subscriber $subscriber)
    {
        $this->subscribers = $subscriber->where('email_verified_at', '!=', null)->get();
    }

    /**
     * Handle the event.
     *
     * @param SendBlogEvent $event
     * @return void
     */
    public function handle(SendBlogEvent $event)
    {
        if ($event->blog->status === 'PUBLISHED') {
            foreach ($this->subscribers as $subscriber) {
                Mail::to($subscriber->email)->queue(new SendBlogMail($event->blog, $subscriber));
            }
        }
    }
}
