<?php

namespace App\Listeners;

use App\Events\BookingEvent;
use App\Mail\BookingMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class BookingListener
{
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';

    /**
     * The time (seconds) before the job should be processed.
     *
     * @var int
     */
    public $delay = 60;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param BookingEvent $event
     * @return void
     */
    public function handle(BookingEvent $event)
    {
        Mail::to($event->booking->email)->queue(new BookingMail($event->booking->fullName,$event->booking->bookingDate));

    }
}
