<?php

namespace App\Listeners;

use App\Events\SendEmailSubscriber;
use App\Mail\ConfirmEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SubscribeListener implements ShouldQueue
{

    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';

    /**
     * The time (seconds) before the job should be processed.
     *
     * @var int
     */
    public $delay = 60;
    /**
     * Create the event listener.
     *
     * @return void
     */
    /**
     * @var
     */

    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param SendEmailSubscriber $event
     * @return void
     */
    public function handle(SendEmailSubscriber $event)
    {
        Mail::to($event->subscriber->email)->queue(new ConfirmEmail($event->subscriber->token));
    }
}
