<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;

class BookingMail extends Mailable
{
    use Queueable, SerializesModels;

    public $fullName;
    public $bookingDate;

    /**
     * Create a new message instance.
     *
     * @param string $fulName
     * @param Carbon $bookingDate
     */
    public function __construct(string $fulName, Carbon $bookingDate)
    {
        $this->fullName = $fulName;
        $this->bookingDate = $bookingDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): BookingMail
    {
        return $this->from(config('mail.from.address'))->subject(config('mail.from.name'))->view('mail.booking',['fullName'=> $this->fullName,'date'=> $this->bookingDate,]);
    }
}
