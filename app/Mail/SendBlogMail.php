<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendBlogMail extends Mailable
{
    use Queueable, SerializesModels;


    public $blog;
    public $subscriber;

    /**
     * Create a new message instance.
     *
     * @param Model $blog
     * @param Model $subscriber
     */
    public function __construct(Model $blog, Model $subscriber)
    {
        $this->blog = $blog;
        $this->subscriber = $subscriber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): SendBlogMail
    {
        return $this->from(config('mail.from.address'))->subject(config('mail.from.name'))->view('mail.sendBlog',['blog'=> $this->blog,'subscriber'=> $this->subscriber,]);
    }
}
