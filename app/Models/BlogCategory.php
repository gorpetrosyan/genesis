<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BlogCategory extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = FALSE;

    /**
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany('App\Models\BlogPost','blog_category_id','id');
    }

}
