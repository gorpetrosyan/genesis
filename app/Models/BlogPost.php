<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BlogCategory;
use App\Events\SendBlogEvent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BlogPost extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $dispatchesEvents = [
        'created'=> SendBlogEvent::class,
        'updated'=> SendBlogEvent::class,
    ];

    /**
     * @return BelongsTo
     */
    public function blogCategory(): BelongsTo
    {
        return $this->belongsTo(BlogCategory::class,'blog_category_id','id');
    }
}
