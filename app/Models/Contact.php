<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Casts\Json;

class Contact extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = FALSE;

    protected $casts = [
        'sliders' => Json::class,
    ];
}
