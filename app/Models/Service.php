<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = FALSE;

    /**
     * Service constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->hash = md5(time());
    }

    public function prices(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
       return $this->hasMany(Price::class,'service_id','id');
    }
}
