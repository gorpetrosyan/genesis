<?php

namespace App\Models;

use App\Events\SendEmailSubscriber;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use HasFactory;

    protected $guarded = [];


    /**\
     * @var array
     *
     */
    protected $dispatchesEvents = [
        'created'=> SendEmailSubscriber::class,
    ];
}
