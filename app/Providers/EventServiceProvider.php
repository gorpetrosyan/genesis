<?php

namespace App\Providers;

use App\Events\SendBlogEvent;
use App\Listeners\BlogSendListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Listeners\SubscribeListener;
use App\Events\SendEmailSubscriber;
use App\Events\BookingEvent;
use App\Listeners\BookingListener;
use function Illuminate\Events\queueable;
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SendEmailSubscriber::class => [
            SubscribeListener::class
        ],
        BookingEvent::class => [
            BookingListener::class
        ],
        SendBlogEvent::class => [
            BlogSendListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
