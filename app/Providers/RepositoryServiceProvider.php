<?php

namespace App\Providers;

use App\Repository\Eloquent\BaseEloquent;
use App\Repository\Interfaces\BaseInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseInterface::class, BaseEloquent::class);
        $models = array(
            'Contact',
            'BlogCategory',
            'GenesisSlider',
            'ContactInfo',
            'Timing',
            'Team',
            'Subscriber',
            'BlogPost',
            'Question',
            'Booking',
            'Service',
            'Price',
            'About',
            'Partner',
            'Statistic',
            'Video',
            'AboutSlider',
        );

        foreach ($models as $model) {
            $this->app->bind("App\Repository\Interfaces\\{$model}Interface", "App\Repository\Eloquent\\{$model}Eloquent");
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
