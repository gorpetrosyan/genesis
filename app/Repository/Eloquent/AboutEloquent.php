<?php


namespace App\Repository\Eloquent;


use App\Models\About;
use App\Repository\Interfaces\AboutInterface;
use Illuminate\Database\Eloquent\Model;

class AboutEloquent implements AboutInterface
{

    protected $about;

    public function __construct(About $about)
    {
        $this->about = $about;
    }

    public function index(string $language): ? Model
    {
        $about = getLanguagePrefixFiled('about', $language);
        return $this->about->select($about)->first();
    }

}
