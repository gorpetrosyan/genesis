<?php


namespace App\Repository\Eloquent;


use App\Models\AboutSlider;
use App\Repository\Interfaces\AboutSliderInterface;
use Illuminate\Database\Eloquent\Collection;

class AboutSliderEloquent implements AboutSliderInterface
{
     protected $aboutSlider;

     public function __construct(AboutSlider $aboutSlider)
     {
         $this->aboutSlider = $aboutSlider;
     }

     public function index(): ?Collection
     {
        return $this->aboutSlider->all();
     }
}
