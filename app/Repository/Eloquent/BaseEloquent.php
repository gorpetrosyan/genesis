<?php


namespace App\Repository\Eloquent;


use App\Repository\Interfaces\BaseInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BaseEloquent implements BaseInterface
{
   protected $model;

    /**
     * BaseEloquent constructor.
     * @param Model $model
     */
  public function __construct(Model $model)
  {
      $this->model = $model;
  }

    /**
     * @return Collection
     */
  public function index(): Collection
  {
      return $this->model->first();
  }

    /**
     * @param array $data
     * @return Model
     */
  public function create(array $data): Model
  {
     return $this->model->create($data);
  }

    /**
     * @param int $id
     * @return Model|null
     */
  public function show(int $id): ?Model
  {
      return $this->model->where('id',$id)->findOrFail();
  }

    /**
     * @param int $id
     * @param array $data
     * @return Model
     */
  public function update(int $id, array $data): Model
  {
      $model =  $this->model->where('id',$id)->findOrFail();
      $model->update($data);
      return $model;
  }

    /**
     * @param int $id
     * @return bool|null
     */
  public function delete(int $id): ?bool
  {
      return  $this->model->where('id',$id)->delete();
  }
}
