<?php


namespace App\Repository\Eloquent;


use App\Models\BlogCategory;
use App\Repository\Interfaces\BlogCategoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class BlogCategoryEloquent implements BlogCategoryInterface
{

    protected $blogCategory;

    public function __construct(BlogCategory $blogCategory)
    {
        $this->blogCategory = $blogCategory;
    }

    public function index(string $language): ?Collection
    {
        $filed = getLanguagePrefixFiled('name', $language);
        return $this->blogCategory->select('id',$filed)->get();
    }
}
