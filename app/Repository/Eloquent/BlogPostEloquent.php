<?php


namespace App\Repository\Eloquent;


use App\Models\BlogPost;
use App\Repository\Interfaces\BlogCategoryInterface;
use App\Repository\Interfaces\BlogPostInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BlogPostEloquent implements BlogPostInterface
{
    protected $blogPost;

    public function __construct(BlogPost $blogPost)
    {
        $this->blogPost = $blogPost;
    }

    public function index($request)
    {
        $language = $request->header('LANGUAGE');
        $categoryName = getLanguagePrefixFiled('name', $language);
        $title = getLanguagePrefixFiled('title', $language);
        $excerpt = getLanguagePrefixFiled('excerpt', $language);
        return $this->blogPost->where('status','PUBLISHED')->select('id',$title,$excerpt,'image','created_at','blog_category_id')->with(['blogCategory' => function ($category) use($categoryName) {
          return  $category->select('id',$categoryName)->get();
        }])->paginate($request->query('limit'));
    }

    public function show(int $id, string $language)
    {
        $title = getLanguagePrefixFiled('title', $language);
        $excerpt = getLanguagePrefixFiled('excerpt', $language);
        $body = getLanguagePrefixFiled('body', $language);
        $categoryName = getLanguagePrefixFiled('name', $language);
        return $this->blogPost->where('status','published')->with(['blogCategory' => function ($category) use($categoryName) {
            return  $category->select('id',$categoryName)->get();
        }])->select($title,$excerpt,$body,'image','seo_title','meta_description','meta_keywords','created_at','blog_category_id')->findorFail($id);
    }

    public function indexByCategory($request, $category_id)
    {
        $language = $request->header('LANGUAGE');
        $categoryName = getLanguagePrefixFiled('name', $language);
        $title = getLanguagePrefixFiled('title', $language);
        $excerpt = getLanguagePrefixFiled('excerpt', $language);
        return $this->blogPost->where([['status','PUBLISHED'],['blog_category_id','=',$category_id]])->select('id',$title,$excerpt,'image','created_at','blog_category_id')->with(['blogCategory' =>  function ($category) use($categoryName) {
           return $category->select('id',$categoryName)->get();
        }])->paginate($request->query('limit'));
    }
}
