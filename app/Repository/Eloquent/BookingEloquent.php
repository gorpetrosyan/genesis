<?php


namespace App\Repository\Eloquent;


use App\Models\Booking;
use App\Repository\Interfaces\BookingInterface;
use Illuminate\Support\Carbon;

class BookingEloquent implements BookingInterface
{
    protected $booking;

    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    public function create(array $data): bool
    {
        $obj = [];
        $obj['fullName'] = $data['fullName'];
        $obj['email'] = $data['email'];
        $obj['phone'] = $data['phone'];
        $obj['message'] = $data['message'];
        $obj['bDay'] = Carbon::create($data['bDay']);
        $obj['bookingDate'] = Carbon::create($data['bookingDate']);
        $obj['service_id'] = $data['service_id'];
        $this->booking->create($obj);
        return true;
    }
}
