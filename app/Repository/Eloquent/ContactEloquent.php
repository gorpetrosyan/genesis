<?php


namespace App\Repository\Eloquent;


use App\Models\Contact;
use App\Repository\Interfaces\ContactInterface;
use Illuminate\Database\Eloquent\Model;

class ContactEloquent implements ContactInterface
{
    protected $contact;

    /**
     * ContactEloquent constructor.
     * @param Contact $model
     */
    public function __construct(Contact $model)
    {
        $this->contact = $model;
    }

    /**
     * @return Model|null
     */
    public function firstItem(): ?Model
    {
        return $this->contact->first() ?? null;
    }
}
