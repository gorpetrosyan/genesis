<?php


namespace App\Repository\Eloquent;


use App\Models\ContactInfo;
use App\Repository\Interfaces\ContactInfoInterface;
use Illuminate\Database\Eloquent\Model;

class ContactInfoEloquent implements ContactInfoInterface
{
    protected $contactInfo;

    public function __construct(ContactInfo $contactInfo)
    {
        $this->contactInfo = $contactInfo;
    }

    public function index(string $language): ?Model
    {
        $title = getLanguagePrefixFiled('title', $language);
        $description = getLanguagePrefixFiled('description', $language);
        return $this->contactInfo->select($title,$description,'image')->first();
    }

}
