<?php


namespace App\Repository\Eloquent;


use App\Models\GenesisSlider;
use App\Repository\Interfaces\GenesisSliderInterface;
use Illuminate\Database\Eloquent\Model;

class GenesisSliderEloquent implements GenesisSliderInterface
{
    private $genesisSlider;

    public function __construct(GenesisSlider $genesisSlider)
    {
        $this->genesisSlider = $genesisSlider;
    }

    public function index(): ?Model
    {
      return $this->genesisSlider->firstOrFail();
    }
}
