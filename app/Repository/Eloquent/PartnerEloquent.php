<?php


namespace App\Repository\Eloquent;


use App\Models\Partner;
use App\Repository\Interfaces\PartnerInterface;
use Illuminate\Database\Eloquent\Collection;

class PartnerEloquent implements PartnerInterface
{

    protected $partner;

    public function __construct(Partner $partner)
    {
        $this->partner = $partner;
    }

    public function index(): ?Collection
    {
        return $this->partner->get();
    }
}
