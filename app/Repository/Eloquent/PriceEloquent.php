<?php


namespace App\Repository\Eloquent;


use App\Models\Price;
use App\Models\Service;
use App\Repository\Interfaces\PriceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PriceEloquent implements PriceInterface
{
    public $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function index($request): ?Collection
    {
        $language = $request->header('LANGUAGE');
        $title = getLanguagePrefixFiled('title', $language);
        $description = getLanguagePrefixFiled('desc', $language);
        return $this->service->select($title,$description,'id', 'hash')->with(['prices'=> function($price) use($title) {
           return $price->select($title,'price','sale','service_id')->get();
        }])->get();
    }
}
