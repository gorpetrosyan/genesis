<?php


namespace App\Repository\Eloquent;


use App\Models\Question;
use App\Repository\Interfaces\QuestionInterface;
use Illuminate\Database\Eloquent\Collection;

class QuestionEloquent implements QuestionInterface
{

    protected $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    public function index(string $language): ?Collection
    {
        $title = getLanguagePrefixFiled('title', $language);
        $description = getLanguagePrefixFiled('desc', $language);
        return $this->question->select($title,$description)->get();
    }

}
