<?php


namespace App\Repository\Eloquent;


use App\Models\Service;
use App\Repository\Interfaces\ServiceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ServiceEloquent implements ServiceInterface
{

    public $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function index($request): ?Collection
    {
        $language = $request->header('LANGUAGE');
        $title = getLanguagePrefixFiled('title', $language);
        $description = getLanguagePrefixFiled('desc', $language);
        return $this->service->select('id','image','hash',$description,$title)->get();
    }

    public function show(string $hash, string $language): ?Model
    {
        $title = getLanguagePrefixFiled('title', $language);
        $description = getLanguagePrefixFiled('desc', $language);
        $about = getLanguagePrefixFiled('about', $language);
        return $this->service->where('hash',$hash)->select('id','image','hash',$title, $description, $about)->firstOrFail();
    }

    public function last(string $language): ?Model
    {
        $title = getLanguagePrefixFiled('title', $language);
        return $this->service->select('hash',$title)->orderBy('id', 'DESC')->first();
    }
}
