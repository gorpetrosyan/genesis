<?php


namespace App\Repository\Eloquent;


use App\Models\Statistic;
use App\Repository\Interfaces\StatisticInterface;
use Illuminate\Database\Eloquent\Collection;

class StatisticEloquent implements StatisticInterface
{
    protected $statistics;

    public function __construct(Statistic $statistics)
    {
        $this->statistics = $statistics;
    }

    public function index(string $language): ?Collection
    {
        $title = getLanguagePrefixFiled('title', $language);
        return $this->statistics->select('count',$title)->get();
    }

}
