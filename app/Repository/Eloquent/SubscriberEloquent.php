<?php


namespace App\Repository\Eloquent;


use App\Events\SendEmailSubscriber;
use App\Models\Subscriber;
use App\Repository\Interfaces\SubscriberInterface;
use Faker\Provider\Uuid;

class SubscriberEloquent implements SubscriberInterface
{

    private $subscriber;

    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function subscribe($email, $language): int
    {
        $subscriber = $this->subscriber->where('email', $email)->first();
        if ($subscriber) {
            if ($subscriber->email_verified_at && $subscriber->token) {
                return 203;
            }else {
                $subscriber->update([
                    'language' => $language,
                    'token' => Uuid::uuid(),
                ]);
                event(new SendEmailSubscriber($subscriber));
                return 201;
            }
        }
        $this->subscriber->create([
            'email' => $email,
            'language' => $language,
            'token' => Uuid::uuid(),
        ]);
        return 201;
    }


    public function unsubscribe($token): int
    {
        $subscriber = $this->subscriber->where('token', $token)->first();
        if ($subscriber && $subscriber['email_verified_at']) {
            $subscriber->update([
                'token' => null,
                'email_verified_at'=> null,
            ]);
            return 200;
        }
        return 404;
    }

    public function confirm($token): int
    {
        $subscriber = $this->subscriber->where('token', $token)->first();
        if ($subscriber && !$subscriber['email_verified_at']) {
            $subscriber->update([
                'email_verified_at' => now(),
            ]);
            return 200;
        }
        return 404;
    }
}
