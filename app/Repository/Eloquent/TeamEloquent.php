<?php


namespace App\Repository\Eloquent;


use App\Models\Team;
use App\Repository\Interfaces\TeamInterface;
use Illuminate\Database\Eloquent\Collection;

class TeamEloquent implements TeamInterface
{
    private $team;

    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    public function index(string $language): ?Collection
    {
        $fullName = getLanguagePrefixFiled('fullName', $language);
        $profession = getLanguagePrefixFiled('profession', $language);
        $description = getLanguagePrefixFiled('description', $language);
        return $this->team->select($fullName,$profession,$description,'image')->get();
    }
}
