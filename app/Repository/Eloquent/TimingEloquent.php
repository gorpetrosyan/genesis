<?php


namespace App\Repository\Eloquent;
use App\Models\Timing;
use App\Repository\Interfaces\TimingInterface;
use Illuminate\Database\Eloquent\Collection;

class TimingEloquent implements TimingInterface
{

    protected $timing;

    public function __construct(Timing $timing)
    {
        $this->timing = $timing;
    }

    public function index(): ?Collection
    {
        return $this->timing->orderBy('day','ASC')
            ->take(7)
            ->get();
    }

}
