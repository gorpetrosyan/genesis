<?php


namespace App\Repository\Eloquent;


use App\Models\Video;
use App\Repository\Interfaces\VideoInterface;
use Illuminate\Database\Eloquent\Model;

class VideoEloquent implements VideoInterface
{

    protected $videoModel;

    public function __construct(Video $video)
    {
        $this->videoModel = $video;
    }

    public function index(string $language): ?Model
    {
        $title = getLanguagePrefixFiled('title', $language);
        $desc = getLanguagePrefixFiled('desc', $language);
        return $this->videoModel->select($title,$desc,'video')->first();
    }
}
