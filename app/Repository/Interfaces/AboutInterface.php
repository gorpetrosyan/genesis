<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface AboutInterface
{

    /**
     * @param string $language
     * @return Model|null
     */
    public function index(string $language): ?Model;

}
