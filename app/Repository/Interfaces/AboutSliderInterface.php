<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface AboutSliderInterface
{
    public function index(): ? Collection;
}
