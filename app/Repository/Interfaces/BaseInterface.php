<?php


namespace App\Repository\Interfaces;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface BaseInterface
{
    /**
     * @return Collection
     */
   public function index(): Collection;

    /**
     * @param array $data
     * @return Model
     */
   public function create(array $data): Model;

    /**
     * @param int $id
     * @return Model|null
     */
   public function show(int $id): ?Model;

    /**
     * @param int $id
     * @param array $data
     * @return Model|null
     */
   public function update(int $id, array $data): ?Model;

    /**
     * @param int $id
     * @return bool|null
     */
   public function delete(int $id): ?bool;
}
