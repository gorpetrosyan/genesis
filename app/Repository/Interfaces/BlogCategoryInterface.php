<?php


namespace App\Repository\Interfaces;


use Illuminate\Support\Collection;

interface BlogCategoryInterface
{
   public function index(string $language): ?Collection;
}
