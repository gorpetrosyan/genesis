<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BlogPostInterface
{
    public function index($request);

    public function indexByCategory($request, $category_id);

    public function show(int $id, string $language);

}
