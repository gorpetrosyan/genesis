<?php


namespace App\Repository\Interfaces;

interface BookingInterface
{
    public function create(array $data): bool;
}
