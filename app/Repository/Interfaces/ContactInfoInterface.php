<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface ContactInfoInterface
{
    public function index(string $language): ?Model;
}
