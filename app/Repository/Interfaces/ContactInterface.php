<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface ContactInterface
{
    /**
     * @return Model|null
     */
    public function firstItem(): ?Model;
}
