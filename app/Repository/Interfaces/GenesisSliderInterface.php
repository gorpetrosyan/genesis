<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface GenesisSliderInterface
{
   public function index(): ?Model;
}
