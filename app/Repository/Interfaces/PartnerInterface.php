<?php


namespace App\Repository\Interfaces;
use Illuminate\Database\Eloquent\Collection;

interface PartnerInterface
{
    /**
     * @return Collection|null
     */
    public function index(): ?Collection;

}
