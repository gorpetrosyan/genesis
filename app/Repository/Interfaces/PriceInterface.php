<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface PriceInterface
{
    public function index($request): ?Collection;
}
