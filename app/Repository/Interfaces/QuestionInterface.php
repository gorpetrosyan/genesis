<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface QuestionInterface
{
    public function index(string $language): ?Collection;
}
