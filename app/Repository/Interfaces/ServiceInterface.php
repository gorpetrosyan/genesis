<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface ServiceInterface
{
    public function index($request): ?Collection;

    public function show(string $hash, string $language): ?Model;

    public function last(string $language): ? Model;
}
