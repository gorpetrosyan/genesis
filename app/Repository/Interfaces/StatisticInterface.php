<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface StatisticInterface
{

    public function index(string $language): ?Collection;

}
