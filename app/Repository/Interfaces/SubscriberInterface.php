<?php


namespace App\Repository\Interfaces;


interface SubscriberInterface
{
  public function subscribe($email,$language): int;

  public function unsubscribe($token): int;

  public function confirm($token): int;
}
