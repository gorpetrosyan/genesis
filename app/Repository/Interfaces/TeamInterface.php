<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface TeamInterface
{
    public function index(string $language): ?Collection;

}
