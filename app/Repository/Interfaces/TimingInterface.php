<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Collection;

interface TimingInterface
{
   public function index(): ? Collection;
}
