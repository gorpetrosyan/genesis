<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface VideoInterface
{
   public function index(string $language): ?Model;
}
