<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('blog_category_id') ->constrained('blog_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');;
            $table->string('title_hy');
            $table->string('title_en')->nullable();
            $table->string('title_ru')->nullable();
            $table->text('excerpt_hy');
            $table->text('excerpt_en')->nullable();
            $table->text('excerpt_ru')->nullable();
            $table->longText('body_hy');
            $table->longText('body_en')->nullable();
            $table->longText('body_ru')->nullable();
            $table->string('image')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->enum('status', ['PUBLISHED', 'DRAFT'])->default('DRAFT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_posts');
    }
}
