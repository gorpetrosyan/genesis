<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('fullName_hy');
            $table->string('fullName_en')->nullable();
            $table->string('fullName_ru')->nullable();
            $table->string('profession_hy');
            $table->string('profession_en')->nullable();
            $table->string('profession_ru')->nullable();
            $table->string('description_hy');
            $table->string('description_en')->nullable();
            $table->string('description_ru')->nullable();
            $table->string('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
