-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2021 at 09:14 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `genesis`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `about_hy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_ru` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `about_sliders`
--

CREATE TABLE `about_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_category_id` bigint(20) UNSIGNED NOT NULL,
  `title_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt_hy` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt_ru` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_hy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `body_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_ru` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bDay` datetime DEFAULT NULL,
  `bookingDate` datetime NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `service_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(2, NULL, 1, 'Category 2', 'category-2', '2021-04-07 05:20:50', '2021-04-07 05:20:50');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_map_link` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `href_map` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_infos`
--

CREATE TABLE `contact_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 11, 'email', 'text', 'Էլ հասցե', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:35\",\"min:7\",\"email\"],\"messages\":{\"required\":\"\\u0537\\u056c \\u0570\\u0561\\u057d\\u0581\\u0565-\\u0576 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0537\\u056c \\u0570\\u0561\\u057d\\u0581\\u0565-\\u0576 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 35 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0537\\u056c \\u0570\\u0561\\u057d\\u0581\\u0565-\\u0576 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 7 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"email\":\"\\u0537\\u056c \\u0570\\u0561\\u057d\\u0581\\u0565\\u056b \\u057d\\u056d\\u0561\\u056c \\u0586\\u0578\\u0580\\u0574\\u0561\\u057f\"}}}', 2),
(58, 11, 'phone', 'text', 'Հեռախոսահամար', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:35\",\"min:7\"],\"messages\":{\"required\":\"\\u0540\\u0565\\u057c\\u0561\\u056d\\u0578\\u057d\\u0561\\u0570\\u0561\\u0574\\u0561\\u0580-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0540\\u0565\\u057c\\u0561\\u056d\\u0578\\u057d\\u0561\\u0570\\u0561\\u0574\\u0561\\u0580-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 35 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0540\\u0565\\u057c\\u0561\\u056d\\u0578\\u057d\\u0561\\u0570\\u0561\\u0574\\u0561\\u0580-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 7 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(59, 11, 'address', 'text', 'Հասցե', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"min:10\",\"max:250\"],\"messages\":{\"required\":\"\\u0540\\u0561\\u057d\\u0581\\u0565-\\u0576 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"min\":\"\\u0540\\u0561\\u057d\\u0581\\u0565-\\u0576 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 10 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"max\":\"\\u0540\\u0561\\u057d\\u0581\\u0565-\\u0576 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 250 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(60, 11, 'facebook', 'text', 'Facebook', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"min:10\",\"url\"],\"messages\":{\"required\":\"Facebook\\u056b \\u0570\\u0572\\u0578\\u0582\\u0574-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"min\":\"Facebook\\u056b \\u0570\\u0572\\u0578\\u0582\\u0574-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 10 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"url\":\"\\u054d\\u056d\\u0561\\u056c \\u0570\\u0572\\u0578\\u0582\\u0574(url)\"}}}', 5),
(61, 11, 'instagram', 'text', 'Instagram', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"min:10\",\"url\"],\"messages\":{\"required\":\"Instagram\\u056b \\u0570\\u0572\\u0578\\u0582\\u0574-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"min\":\"Instagram\\u056b \\u0570\\u0572\\u0578\\u0582\\u0574-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 10 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"url\":\"\\u054d\\u056d\\u0561\\u056c \\u0570\\u0572\\u0578\\u0582\\u0574(url)\"}}}', 6),
(62, 11, 'address_map_link', 'text_area', 'Քարտեզի հղում', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"min:10\",\"url\"],\"messages\":{\"required\":\"\\u0554\\u0561\\u0580\\u057f\\u0565\\u0566\\u056b \\u0570\\u0572\\u0578\\u0582\\u0574-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"min\":\"\\u0554\\u0561\\u0580\\u057f\\u0565\\u0566\\u056b \\u0570\\u0572\\u0578\\u0582\\u0574-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 10 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"url\":\"\\u054d\\u056d\\u0561\\u056c \\u0570\\u0572\\u0578\\u0582\\u0574(url)\"}}}', 7),
(63, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(64, 13, 'name_hy', 'text', 'Անվանում Am', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:55\",\"min:3\"],\"messages\":{\"required\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Am-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 2),
(65, 13, 'name_ru', 'text', 'Անվանում  Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:55\",\"min:3\"],\"messages\":{\"max\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(66, 13, 'name_en', 'text', 'Անվանում En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:55\",\"min:3\"],\"messages\":{\"max\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(67, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(70, 15, 'title_hy', 'text', 'Վերնագիր Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:85\",\"min:3\"],\"messages\":{\"required\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(71, 15, 'title_en', 'text', 'Վերնագիր En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:85\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(72, 15, 'title_ru', 'text', 'Վերնագիր Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:85\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 6),
(73, 15, 'excerpt_hy', 'text_area', 'Նկարագրություն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:300\",\"min:3\"],\"messages\":{\"required\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 7),
(74, 15, 'excerpt_en', 'text_area', 'Նկարագրություն En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 8),
(75, 15, 'excerpt_ru', 'text_area', 'Նկարագրություն Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 9),
(76, 15, 'body_hy', 'rich_text_box', 'Տեքստ Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"min:3\"],\"messages\":{\"required\":\"\\u054f\\u0565\\u0584\\u057d\\u057f Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"min\":\"\\u054f\\u0565\\u0584\\u057d\\u057f Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 10),
(77, 15, 'body_en', 'rich_text_box', 'Տեքստ En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"min:3\"],\"messages\":{\"min\":\"\\u054f\\u0565\\u0584\\u057d\\u057f En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 11),
(78, 15, 'body_ru', 'rich_text_box', 'Տեքստ Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"min:3\"],\"messages\":{\"min\":\"\\u054f\\u0565\\u0584\\u057d\\u057f Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 12),
(79, 15, 'image', 'image', 'Նկար', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 13),
(80, 15, 'seo_title', 'text', 'Seo Վերնագիր', 0, 1, 1, 1, 1, 1, '{}', 14),
(81, 15, 'meta_description', 'text_area', 'Meta Նկարագիր', 0, 1, 1, 1, 1, 1, '{}', 15),
(82, 15, 'meta_keywords', 'text_area', 'Meta Keywords', 0, 1, 1, 1, 1, 1, '{}', 16),
(83, 15, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\"}}', 17),
(84, 15, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 18),
(85, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 19),
(87, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(89, 17, 'sliders', 'multiple_images', 'Նկարներ', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"],\"messages\":{\"required\":\"\\u0546\\u056f\\u0561\\u0580\\u0576\\u0565\\u0580-\\u0576 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\"}}}', 3),
(90, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(91, 18, 'title_hy', 'text', 'Վերնագիր Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:55\",\"min:3\"],\"messages\":{\"required\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 2),
(92, 18, 'title_ru', 'text', 'Վերնագիր Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:55\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(93, 18, 'title_en', 'text', 'Վերնագիր En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:55\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(94, 18, 'description_hy', 'text_area', 'Նկարագրություն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:55\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(95, 18, 'description_ru', 'text_area', 'Նկարագրություն  Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 6),
(96, 18, 'description_en', 'text_area', 'Նկարագրություն  En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 7),
(97, 18, 'image', 'image', 'Նկար', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 8),
(98, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(99, 19, 'day', 'number', 'Օր(1-7)', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"numeric\"],\"messages\":{\"required\":\"\\u0555\\u0580-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"numeric\":\"\\u0555\\u0580-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0569\\u057e\\u0565\\u0580\"}}}', 2),
(100, 19, 'start', 'time', 'Սկիզբ', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\"]}}', 3),
(101, 19, 'end', 'time', 'Ավարտ', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\"]}}', 4),
(102, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(103, 20, 'fullName_hy', 'text', 'Անուն Ազգանուն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:65\",\"min:3\"],\"messages\":{\"required\":\"\\u0531\\u0576\\u0578\\u0582\\u0576 \\u0531\\u0566\\u0563\\u0561\\u0576\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0531\\u0576\\u0578\\u0582\\u0576 \\u0531\\u0566\\u0563\\u0561\\u0576\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 65 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u0578\\u0582\\u0576 \\u0531\\u0566\\u0563\\u0561\\u0576\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 2),
(104, 20, 'fullName_en', 'text', 'Անուն Ազգանուն En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:65\",\"min:3\"],\"messages\":{\"max\":\"\\u0531\\u0576\\u0578\\u0582\\u0576 \\u0531\\u0566\\u0563\\u0561\\u0576\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 65 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u0578\\u0582\\u0576 \\u0531\\u0566\\u0563\\u0561\\u0576\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(105, 20, 'fullName_ru', 'text', 'Անուն Ազգանուն Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:65\",\"min:3\"],\"messages\":{\"max\":\"\\u0531\\u0576\\u0578\\u0582\\u0576 \\u0531\\u0566\\u0563\\u0561\\u0576\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 65 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u0578\\u0582\\u0576 \\u0531\\u0566\\u0563\\u0561\\u0576\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(106, 20, 'profession_hy', 'text', 'Պաշտոն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:55\",\"min:3\"],\"messages\":{\"required\":\"\\u054a\\u0561\\u0577\\u057f\\u0578\\u0576 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u054a\\u0561\\u0577\\u057f\\u0578\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 55 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054a\\u0561\\u0577\\u057f\\u0578\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(107, 20, 'profession_en', 'text', 'Պաշտոն En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:80\",\"min:3\"],\"messages\":{\"max\":\"\\u054a\\u0561\\u0577\\u057f\\u0578\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 80 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054a\\u0561\\u0577\\u057f\\u0578\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 6),
(108, 20, 'profession_ru', 'text', 'Պաշտոն Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:80\",\"min:3\"],\"messages\":{\"max\":\"\\u054a\\u0561\\u0577\\u057f\\u0578\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 80 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054a\\u0561\\u0577\\u057f\\u0578\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 7),
(109, 20, 'description_hy', 'text_area', 'Նկարագրություն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:350\",\"min:3\"],\"messages\":{\"required\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 350 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 8),
(110, 20, 'description_en', 'text_area', 'Նկարագրություն En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:350\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 350 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 9),
(111, 20, 'description_ru', 'text_area', 'Նկարագրություն Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:350\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 350 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 10),
(112, 20, 'image', 'image', 'Նկար', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 11),
(114, 15, 'blog_category_id', 'select_dropdown', 'Կատեգորիա', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"],\"messages\":{\"required\":\"\\u053f\\u0561\\u057f\\u0565\\u0563\\u0578\\u0580\\u056b\\u0561-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\"}}}', 2),
(115, 15, 'blog_post_belongsto_blog_category_relationship', 'relationship', 'Կատեգորիա', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\BlogCategory\",\"table\":\"blog_categories\",\"type\":\"belongsTo\",\"column\":\"blog_category_id\",\"key\":\"id\",\"label\":\"name_hy\",\"pivot_table\":\"blog_categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 20),
(116, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(117, 21, 'title_hy', 'text', 'Վերնագիր Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:100\",\"min:3\"],\"messages\":{\"required\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 100 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 2),
(118, 21, 'title_en', 'text', 'Վերնագիր En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:100\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 100 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(119, 21, 'title_ru', 'text', 'Վերնագիր Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:100\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 100 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(120, 21, 'desc_hy', 'text_area', 'Նկարագրություն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:450\",\"min:3\"],\"messages\":{\"required\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 450 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(121, 21, 'desc_en', 'text_area', 'Նկարագրություն  En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:450\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 450 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 6),
(122, 21, 'desc_ru', 'text_area', 'Նկարագրություն  Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:450\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 450 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 7),
(123, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(124, 22, 'fullName', 'text', 'Անուն', 1, 1, 1, 0, 0, 1, '{}', 2),
(125, 22, 'email', 'text', 'Email', 1, 1, 1, 0, 0, 1, '{}', 3),
(126, 22, 'phone', 'text', 'Հեռ․', 1, 1, 1, 0, 0, 1, '{}', 4),
(127, 22, 'bDay', 'text', 'Ծննդ․ ԱԱ', 0, 1, 1, 0, 0, 1, '{}', 5),
(128, 22, 'bookingDate', 'text', 'Ամրագրման օր', 1, 1, 1, 0, 0, 1, '{}', 6),
(129, 22, 'message', 'text', 'Նամակ', 0, 1, 1, 0, 0, 1, '{}', 7),
(130, 22, 'status', 'checkbox', 'Կարգավիճակ', 1, 1, 1, 1, 0, 1, '{}', 8),
(131, 22, 'created_at', 'timestamp', 'Ուղարկվել է', 0, 1, 1, 0, 0, 1, '{}', 9),
(132, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(133, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(134, 23, 'email', 'text', 'Email', 1, 1, 1, 0, 0, 1, '{}', 2),
(135, 23, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 0, 0, 0, '{}', 3),
(136, 23, 'language', 'text', 'Language', 1, 1, 1, 0, 0, 1, '{}', 4),
(137, 23, 'token', 'text', 'Token', 0, 0, 1, 0, 0, 0, '{}', 5),
(138, 23, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(139, 23, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 7),
(140, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(141, 24, 'hash', 'text', 'Hash', 1, 0, 1, 0, 0, 0, '{}', 2),
(142, 24, 'title_hy', 'text', 'Վերնագիր Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:85\",\"min:3\"],\"messages\":{\"required\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(143, 24, 'title_en', 'text', 'Վերնագիր En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:85\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(144, 24, 'title_ru', 'text', 'Վերնագիր Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:85\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(145, 24, 'desc_hy', 'text_area', 'Նկարագրություն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:300\",\"min:3\"],\"messages\":{\"required\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 6),
(146, 24, 'desc_ru', 'text_area', 'Նկարագրություն Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 7),
(147, 24, 'desc_en', 'text_area', 'Նկարագրություն  En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 8),
(148, 24, 'image', 'image', 'Նկար', 1, 1, 1, 1, 1, 1, '{}', 9),
(149, 24, 'about_hy', 'rich_text_box', 'Տեքստ Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"min:3\"],\"messages\":{\"required\":\"\\u054f\\u0565\\u0584\\u057d\\u057f Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"min\":\"\\u054f\\u0565\\u0584\\u057d\\u057f Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 10),
(150, 24, 'about_ru', 'rich_text_box', 'Տեքստ Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"min:3\"],\"messages\":{\"min\":\"\\u054f\\u0565\\u0584\\u057d\\u057f Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 11),
(151, 24, 'about_en', 'rich_text_box', 'Տեքստ En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"min:3\"],\"messages\":{\"min\":\"\\u054f\\u0565\\u0584\\u057d\\u057f En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 12),
(152, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(153, 25, 'title_hy', 'text_area', 'Վերնագիր  Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:200\",\"min:3\"],\"messages\":{\"required\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 200 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3);
INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(154, 25, 'title_en', 'text_area', 'Վերնագիր  En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:200\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 200 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(155, 25, 'title_ru', 'text_area', 'Վերնագիր  Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:200\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 200 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(156, 25, 'price', 'text', 'Գին', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"numeric\",\"min:1\"],\"messages\":{\"required\":\"\\u0533\\u056b\\u0576-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"numeric\":\"\\u0533\\u056b\\u0576-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0569\\u057e\\u0565\\u0580\",\"min\":\"\\u0533\\u056b\\u0576-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 1 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 6),
(157, 25, 'sale', 'text', 'Զեղջ', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"numeric\",\"min:1\"],\"messages\":{\"numeric\":\"\\u0536\\u0565\\u0572\\u057b-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0569\\u057e\\u0565\\u0580\",\"min\":\"\\u0536\\u0565\\u0572\\u057b-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 1 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 7),
(158, 25, 'service_id', 'select_dropdown', 'Ծառայություն', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"],\"messages\":{\"required\":\"\\u053e\\u0561\\u057c\\u0561\\u0575\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\"}}}', 2),
(159, 25, 'price_belongsto_service_relationship', 'relationship', 'Ծառայություն', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"title_hy\",\"pivot_table\":\"blog_categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(160, 22, 'service_id', 'select_dropdown', 'Ծառայություն', 1, 1, 1, 0, 0, 1, '{}', 2),
(161, 22, 'booking_belongsto_service_relationship', 'relationship', 'Ծառայություն', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"title_hy\",\"pivot_table\":\"blog_categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(162, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(163, 26, 'about_hy', 'rich_text_box', 'Նկարագիր Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"min:3\"],\"messages\":{\"required\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 2),
(164, 26, 'about_ru', 'rich_text_box', 'Նկարագիր Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"min:3\"],\"messages\":{\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(165, 26, 'about_en', 'rich_text_box', 'Նկարագիր En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"min:3\"],\"messages\":{\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(166, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(167, 27, 'image', 'image', 'Նկար', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"280\",\"height\":\"180\"},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"280\",\"height\":\"180\"}}]}', 2),
(168, 28, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(169, 28, 'count', 'number', 'Տվյալ', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"numeric\"],\"messages\":{\"required\":\"\\u054f\\u057e\\u0575\\u0561\\u056c-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"numeric\":\"\\u054f\\u057e\\u0575\\u0561\\u056c-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0569\\u057e\\u0561\\u0575\\u056b\\u0576 \\u0561\\u0580\\u056a\\u0565\\u0584\"}}}', 2),
(170, 28, 'title_hy', 'text', 'Անվանում Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:75\",\"min:3\"],\"messages\":{\"required\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 75 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(171, 28, 'title_en', 'text', 'Անվանում En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:75\",\"min:3\"],\"messages\":{\"max\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 75 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(172, 28, 'title_ru', 'text', 'Անվանում Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:75\",\"min:3\"],\"messages\":{\"max\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 75 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0531\\u0576\\u057e\\u0561\\u0576\\u0578\\u0582\\u0574 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(173, 29, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(174, 29, 'video', 'media_picker', 'Վիդեո', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\"],\"messages\":{\"required\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\"}}}', 2),
(175, 29, 'title_hy', 'text', 'Վերնագիր Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:85\",\"min:3\"],\"messages\":{\"required\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Am-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 3),
(176, 29, 'title_en', 'text', 'Վերնագիր En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:85\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 4),
(177, 29, 'title_ru', 'text', 'Վերնագիր Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:85\",\"min:3\"],\"messages\":{\"max\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 85 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u054e\\u0565\\u0580\\u0576\\u0561\\u0563\\u056b\\u0580 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 5),
(178, 29, 'desc_hy', 'text_area', 'Նկարագրություն Hy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"required\",\"max:300\",\"min:3\"],\"messages\":{\"required\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0561\\u0580\\u057f\\u0561\\u0564\\u056b\\u0580 \\u056c\\u0580\\u0561\\u0581\\u0574\\u0561\\u0576 \\u0564\\u0561\\u0577\\u057f \\u0567\",\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Hy-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 6),
(179, 29, 'desc_ru', 'text_area', 'Նկարագրություն Ru', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 Ru-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 7),
(180, 29, 'desc_en', 'text_area', 'Նկարագրություն En', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":[\"nullable\",\"max:300\",\"min:3\"],\"messages\":{\"max\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u0561\\u0584\\u057d\\u056b\\u0574\\u0578\\u0582\\u0574 300 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\",\"min\":\"\\u0546\\u056f\\u0561\\u0580\\u0561\\u0563\\u0580\\u0578\\u0582\\u0569\\u0575\\u0578\\u0582\\u0576 En-\\u0568 \\u057a\\u0565\\u057f\\u0584 \\u0567 \\u057a\\u0561\\u0580\\u0578\\u0582\\u0576\\u0561\\u056f\\u056b \\u0574\\u056b\\u0576\\u056b\\u0574\\u0578\\u0582\\u0574 3 \\u057d\\u056b\\u0574\\u057e\\u0578\\u056c\"}}}', 8),
(181, 30, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(182, 30, 'image', 'image', 'Նկար', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"height\":\"450\"},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-04-07 05:20:47', '2021-04-07 05:20:47'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-04-07 05:20:47', '2021-04-07 05:20:47'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-04-07 05:20:47', '2021-04-07 05:20:47'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(11, 'contacts', 'contacts', 'Contact', 'Կոնտակտներ', 'voyager-telephone', 'App\\Models\\Contact', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-08 13:53:17', '2021-04-08 13:53:17'),
(13, 'blog_categories', 'blog-categories', 'Blog Category', 'Կատեգորիաներ', 'voyager-bookmark', 'App\\Models\\BlogCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-11 11:58:43', '2021-04-11 11:58:43'),
(15, 'blog_posts', 'blog-posts', 'Blog Post', 'Բլոգ փոստեր', 'voyager-logbook', 'App\\Models\\BlogPost', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-04-11 15:48:49', '2021-06-13 11:05:57'),
(17, 'genesis_sliders', 'genesis-sliders', 'Genesis Slider', 'TV Slider', 'voyager-tv', 'App\\Models\\GenesisSlider', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-12 13:27:54', '2021-04-12 14:41:45'),
(18, 'contact_infos', 'contact-infos', 'Contact Info', 'Նկարագիր', 'voyager-info-circled', 'App\\Models\\ContactInfo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-13 15:26:50', '2021-04-13 15:26:50'),
(19, 'timings', 'timings', 'Timing', 'Աշխատանքային Ժամեր', 'voyager-alarm-clock', 'App\\Models\\Timing', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-14 14:50:55', '2021-04-14 15:30:34'),
(20, 'teams', 'teams', 'Team', 'Թիմ', 'voyager-people', 'App\\Models\\Team', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-17 13:09:44', '2021-04-17 13:09:44'),
(21, 'questions', 'questions', 'Question', 'Ամրագրման Հարցեր', 'voyager-key', 'App\\Models\\Question', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-03 17:28:04', '2021-05-03 17:34:59'),
(22, 'bookings', 'bookings', 'Booking', 'Ամրագրումներ', 'voyager-logbook', 'App\\Models\\Booking', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"created_at\",\"order_direction\":\"asc\",\"default_search_key\":\"status\",\"scope\":null}', '2021-05-04 16:05:03', '2021-05-16 14:06:44'),
(23, 'subscribers', 'subscribers', 'Subscriber', 'Բաժանորդներ', 'voyager-people', 'App\\Models\\Subscriber', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-16 07:01:24', '2021-05-16 07:21:24'),
(24, 'services', 'services', 'Service', 'Ծառայություններ', 'voyager-logbook', 'App\\Models\\Service', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-16 07:08:43', '2021-05-16 07:20:01'),
(25, 'prices', 'prices', 'Price', 'Գներ', 'voyager-dollar', 'App\\Models\\Price', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-16 07:30:20', '2021-06-06 13:45:46'),
(26, 'abouts', 'abouts', 'About', 'Մեր մասին', 'voyager-font', 'App\\Models\\About', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-13 04:57:00', '2021-06-13 04:59:53'),
(27, 'partners', 'partners', 'Partner', 'Գործընկերներ', 'voyager-receipt', 'App\\Models\\Partner', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-13 06:35:11', '2021-06-13 06:55:45'),
(28, 'statistics', 'statistics', 'Statistic', 'Ստատիստիկա', 'voyager-bar-chart', 'App\\Models\\Statistic', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-06-13 11:07:41', '2021-06-13 11:07:41'),
(29, 'videos', 'videos', 'Video', 'Վիդեո', 'voyager-video', 'App\\Models\\Video', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-13 18:00:37', '2021-06-13 18:09:59'),
(30, 'about_sliders', 'about-sliders', 'About Slider', 'Սլայդեր', 'voyager-images', 'App\\Models\\AboutSlider', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-06-14 15:57:58', '2021-06-14 16:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `genesis_sliders`
--

CREATE TABLE `genesis_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sliders` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`sliders`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genesis_sliders`
--

INSERT INTO `genesis_sliders` (`id`, `sliders`) VALUES
(1, '[\"genesis-sliders\\\\April2021\\\\jOSbgxbBOpQLLfAKEver.jpg\",\"genesis-sliders\\\\April2021\\\\dTMIT8SwBDv6wdCwd8ts.jpg\",\"genesis-sliders\\\\April2021\\\\wK6IT9FlAF20zrO2BI9D.jpg\"]');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-04-07 05:20:48', '2021-04-07 05:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-04-07 05:20:48', '2021-04-07 05:20:48', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2021-04-07 05:20:48', '2021-04-08 13:54:03', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2021-04-07 05:20:48', '2021-04-07 05:20:48', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-04-07 05:20:48', '2021-04-07 05:20:48', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 17, '2021-04-07 05:20:48', '2021-06-14 15:59:04', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2021-04-07 05:20:48', '2021-04-14 14:54:04', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2021-04-07 05:20:48', '2021-04-14 14:54:04', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2021-04-07 05:20:48', '2021-04-14 14:54:04', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2021-04-07 05:20:48', '2021-04-14 14:54:04', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 18, '2021-04-07 05:20:48', '2021-06-14 15:59:04', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 7, '2021-04-07 05:20:50', '2021-04-11 11:58:58', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2021-04-07 05:20:50', '2021-04-08 13:54:03', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2021-04-07 05:20:51', '2021-04-11 11:58:58', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2021-04-07 05:20:51', '2021-04-14 14:54:04', 'voyager.hooks', NULL),
(15, 1, 'Կոնտակտներ', '', '_self', 'voyager-telephone', NULL, 20, 2, '2021-04-08 13:53:17', '2021-04-13 15:27:28', 'voyager.contacts.index', NULL),
(16, 1, 'Կատեգորիաներ', '', '_self', 'voyager-bookmark', NULL, 17, 1, '2021-04-11 11:58:44', '2021-04-11 12:00:02', 'voyager.blog-categories.index', NULL),
(17, 1, 'Բլոգ', '', '_self', 'voyager-book', '#000000', NULL, 11, '2021-04-11 11:59:46', '2021-06-13 18:01:05', NULL, ''),
(18, 1, 'Բլոգ փոստեր', '', '_self', 'voyager-logbook', NULL, 17, 2, '2021-04-11 15:48:50', '2021-04-11 15:49:09', 'voyager.blog-posts.index', NULL),
(19, 1, 'TV Slider', '', '_self', 'voyager-tv', NULL, NULL, 8, '2021-04-12 13:27:54', '2021-04-13 14:58:38', 'voyager.genesis-sliders.index', NULL),
(20, 1, 'Կոնտակ', '', '_self', 'voyager-location', '#000000', NULL, 10, '2021-04-13 14:58:23', '2021-06-13 06:36:39', NULL, ''),
(21, 1, 'Նկարագիր', '', '_self', 'voyager-info-circled', NULL, 20, 1, '2021-04-13 15:26:50', '2021-04-13 15:27:28', 'voyager.contact-infos.index', NULL),
(22, 1, 'Աշխատանքային Ժամեր', '', '_self', 'voyager-alarm-clock', NULL, 20, 3, '2021-04-14 14:50:55', '2021-04-14 14:54:04', 'voyager.timings.index', NULL),
(23, 1, 'Թիմ', '', '_self', 'voyager-people', NULL, NULL, 15, '2021-04-17 13:09:44', '2021-06-14 15:59:04', 'voyager.teams.index', NULL),
(24, 1, 'Ամրագրում', '', '_self', 'voyager-window-list', '#000000', NULL, 16, '2021-05-03 17:20:58', '2021-06-14 15:59:04', NULL, ''),
(25, 1, 'Ամրագրման Հարցեր', '', '_self', 'voyager-key', NULL, 24, 1, '2021-05-03 17:28:04', '2021-05-03 17:28:17', 'voyager.questions.index', NULL),
(26, 1, 'Ամրագրումներ', '', '_self', 'voyager-logbook', NULL, 24, 2, '2021-05-04 16:05:03', '2021-05-16 07:33:42', 'voyager.bookings.index', NULL),
(27, 1, 'Բաժանորդներ', '', '_self', 'voyager-people', '#000000', NULL, 12, '2021-05-16 07:01:24', '2021-06-13 18:01:05', 'voyager.subscribers.index', 'null'),
(28, 1, 'Տեսակներ', '', '_self', 'voyager-logbook', '#000000', 30, 1, '2021-05-16 07:08:43', '2021-05-16 07:36:22', 'voyager.services.index', 'null'),
(29, 1, 'Գներ', '', '_self', 'voyager-dollar', NULL, 30, 2, '2021-05-16 07:30:20', '2021-05-16 07:35:58', 'voyager.prices.index', NULL),
(30, 1, 'Ծառայույթյուններ', '', '_self', 'voyager-medal-rank-star', '#000000', NULL, 13, '2021-05-16 07:35:37', '2021-06-13 18:01:05', NULL, ''),
(31, 1, 'Մեր մասին', '', '_self', 'voyager-font', NULL, 37, 2, '2021-06-13 04:57:01', '2021-06-14 15:59:04', 'voyager.abouts.index', NULL),
(32, 1, 'Գործընկերներ', '', '_self', 'voyager-receipt', NULL, 33, 3, '2021-06-13 06:35:11', '2021-06-13 18:01:19', 'voyager.partners.index', NULL),
(33, 1, 'Գլխավոր', '', '_self', 'voyager-browser', '#000000', NULL, 9, '2021-06-13 06:36:12', '2021-06-13 06:36:39', NULL, ''),
(34, 1, 'Ստատիստիկա', '', '_self', 'voyager-bar-chart', NULL, 33, 2, '2021-06-13 11:07:41', '2021-06-13 18:01:19', 'voyager.statistics.index', NULL),
(35, 1, 'Վիդեո', '', '_self', 'voyager-video', NULL, 33, 1, '2021-06-13 18:00:38', '2021-06-13 18:01:19', 'voyager.videos.index', NULL),
(36, 1, 'Սլայդեր', '', '_self', 'voyager-images', NULL, 37, 1, '2021-06-14 15:57:58', '2021-06-14 15:58:47', 'voyager.about-sliders.index', NULL),
(37, 1, 'Մեր Մասին', '', '_self', 'voyager-file-text', '#000000', NULL, 14, '2021-06-14 15:58:40', '2021-06-14 15:59:04', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2016_01_01_000000_create_pages_table', 2),
(25, '2016_01_01_000000_create_posts_table', 2),
(26, '2016_02_15_204651_create_categories_table', 2),
(27, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(28, '2021_04_08_173600_create_contacts_table', 3),
(29, '2021_04_11_155125_create_blog_categories_table', 4),
(34, '2021_04_12_170448_create_genesis_sliders_table', 7),
(35, '2021_04_13_190625_create_contact_infos_table', 8),
(37, '2021_04_14_182819_create_timings_table', 9),
(38, '2021_04_17_162428_create_teams_table', 10),
(42, '2021_04_11_190634_create_blog_posts_table', 12),
(43, '2021_05_03_205427_create_questions_table', 13),
(49, '2021_05_14_185856_create_services_table', 15),
(50, '2021_04_11_191321_create_subscribers_table', 16),
(51, '2021_05_16_100145_create_prices_table', 17),
(52, '2021_05_04_182727_create_bookings_table', 18),
(53, '2021_06_13_083731_create_abouts_table', 19),
(54, '2021_06_13_102301_create_partners_table', 20),
(56, '2021_06_13_143951_create_statistics_table', 21),
(57, '2021_06_13_213815_create_videos_table', 22),
(58, '2021_06_14_193118_create_about_sliders_table', 23);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2021-04-07 05:20:51', '2021-04-07 05:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(2, 'browse_bread', NULL, '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(3, 'browse_database', NULL, '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(4, 'browse_media', NULL, '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(5, 'browse_compass', NULL, '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(6, 'browse_menus', 'menus', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(7, 'read_menus', 'menus', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(8, 'edit_menus', 'menus', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(9, 'add_menus', 'menus', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(10, 'delete_menus', 'menus', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(11, 'browse_roles', 'roles', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(12, 'read_roles', 'roles', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(13, 'edit_roles', 'roles', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(14, 'add_roles', 'roles', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(15, 'delete_roles', 'roles', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(16, 'browse_users', 'users', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(17, 'read_users', 'users', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(18, 'edit_users', 'users', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(19, 'add_users', 'users', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(20, 'delete_users', 'users', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(21, 'browse_settings', 'settings', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(22, 'read_settings', 'settings', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(23, 'edit_settings', 'settings', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(24, 'add_settings', 'settings', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(25, 'delete_settings', 'settings', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(26, 'browse_categories', 'categories', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(27, 'read_categories', 'categories', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(28, 'edit_categories', 'categories', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(29, 'add_categories', 'categories', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(30, 'delete_categories', 'categories', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(31, 'browse_posts', 'posts', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(32, 'read_posts', 'posts', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(33, 'edit_posts', 'posts', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(34, 'add_posts', 'posts', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(35, 'delete_posts', 'posts', '2021-04-07 05:20:50', '2021-04-07 05:20:50'),
(36, 'browse_pages', 'pages', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(37, 'read_pages', 'pages', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(38, 'edit_pages', 'pages', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(39, 'add_pages', 'pages', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(40, 'delete_pages', 'pages', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(41, 'browse_hooks', NULL, '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(42, 'browse_contacts', 'contacts', '2021-04-08 13:53:17', '2021-04-08 13:53:17'),
(43, 'read_contacts', 'contacts', '2021-04-08 13:53:17', '2021-04-08 13:53:17'),
(44, 'edit_contacts', 'contacts', '2021-04-08 13:53:17', '2021-04-08 13:53:17'),
(45, 'add_contacts', 'contacts', '2021-04-08 13:53:17', '2021-04-08 13:53:17'),
(46, 'delete_contacts', 'contacts', '2021-04-08 13:53:17', '2021-04-08 13:53:17'),
(47, 'browse_blog_categories', 'blog_categories', '2021-04-11 11:58:44', '2021-04-11 11:58:44'),
(48, 'read_blog_categories', 'blog_categories', '2021-04-11 11:58:44', '2021-04-11 11:58:44'),
(49, 'edit_blog_categories', 'blog_categories', '2021-04-11 11:58:44', '2021-04-11 11:58:44'),
(50, 'add_blog_categories', 'blog_categories', '2021-04-11 11:58:44', '2021-04-11 11:58:44'),
(51, 'delete_blog_categories', 'blog_categories', '2021-04-11 11:58:44', '2021-04-11 11:58:44'),
(52, 'browse_blog_posts', 'blog_posts', '2021-04-11 15:48:50', '2021-04-11 15:48:50'),
(53, 'read_blog_posts', 'blog_posts', '2021-04-11 15:48:50', '2021-04-11 15:48:50'),
(54, 'edit_blog_posts', 'blog_posts', '2021-04-11 15:48:50', '2021-04-11 15:48:50'),
(55, 'add_blog_posts', 'blog_posts', '2021-04-11 15:48:50', '2021-04-11 15:48:50'),
(56, 'delete_blog_posts', 'blog_posts', '2021-04-11 15:48:50', '2021-04-11 15:48:50'),
(57, 'browse_genesis_sliders', 'genesis_sliders', '2021-04-12 13:27:54', '2021-04-12 13:27:54'),
(58, 'read_genesis_sliders', 'genesis_sliders', '2021-04-12 13:27:54', '2021-04-12 13:27:54'),
(59, 'edit_genesis_sliders', 'genesis_sliders', '2021-04-12 13:27:54', '2021-04-12 13:27:54'),
(60, 'add_genesis_sliders', 'genesis_sliders', '2021-04-12 13:27:54', '2021-04-12 13:27:54'),
(61, 'delete_genesis_sliders', 'genesis_sliders', '2021-04-12 13:27:54', '2021-04-12 13:27:54'),
(62, 'browse_contact_infos', 'contact_infos', '2021-04-13 15:26:50', '2021-04-13 15:26:50'),
(63, 'read_contact_infos', 'contact_infos', '2021-04-13 15:26:50', '2021-04-13 15:26:50'),
(64, 'edit_contact_infos', 'contact_infos', '2021-04-13 15:26:50', '2021-04-13 15:26:50'),
(65, 'add_contact_infos', 'contact_infos', '2021-04-13 15:26:50', '2021-04-13 15:26:50'),
(66, 'delete_contact_infos', 'contact_infos', '2021-04-13 15:26:50', '2021-04-13 15:26:50'),
(67, 'browse_timings', 'timings', '2021-04-14 14:50:55', '2021-04-14 14:50:55'),
(68, 'read_timings', 'timings', '2021-04-14 14:50:55', '2021-04-14 14:50:55'),
(69, 'edit_timings', 'timings', '2021-04-14 14:50:55', '2021-04-14 14:50:55'),
(70, 'add_timings', 'timings', '2021-04-14 14:50:55', '2021-04-14 14:50:55'),
(71, 'delete_timings', 'timings', '2021-04-14 14:50:55', '2021-04-14 14:50:55'),
(72, 'browse_teams', 'teams', '2021-04-17 13:09:44', '2021-04-17 13:09:44'),
(73, 'read_teams', 'teams', '2021-04-17 13:09:44', '2021-04-17 13:09:44'),
(74, 'edit_teams', 'teams', '2021-04-17 13:09:44', '2021-04-17 13:09:44'),
(75, 'add_teams', 'teams', '2021-04-17 13:09:44', '2021-04-17 13:09:44'),
(76, 'delete_teams', 'teams', '2021-04-17 13:09:44', '2021-04-17 13:09:44'),
(77, 'browse_questions', 'questions', '2021-05-03 17:28:04', '2021-05-03 17:28:04'),
(78, 'read_questions', 'questions', '2021-05-03 17:28:04', '2021-05-03 17:28:04'),
(79, 'edit_questions', 'questions', '2021-05-03 17:28:04', '2021-05-03 17:28:04'),
(80, 'add_questions', 'questions', '2021-05-03 17:28:04', '2021-05-03 17:28:04'),
(81, 'delete_questions', 'questions', '2021-05-03 17:28:04', '2021-05-03 17:28:04'),
(82, 'browse_bookings', 'bookings', '2021-05-04 16:05:03', '2021-05-04 16:05:03'),
(83, 'read_bookings', 'bookings', '2021-05-04 16:05:03', '2021-05-04 16:05:03'),
(84, 'edit_bookings', 'bookings', '2021-05-04 16:05:03', '2021-05-04 16:05:03'),
(85, 'add_bookings', 'bookings', '2021-05-04 16:05:03', '2021-05-04 16:05:03'),
(86, 'delete_bookings', 'bookings', '2021-05-04 16:05:03', '2021-05-04 16:05:03'),
(87, 'browse_subscribers', 'subscribers', '2021-05-16 07:01:24', '2021-05-16 07:01:24'),
(88, 'read_subscribers', 'subscribers', '2021-05-16 07:01:24', '2021-05-16 07:01:24'),
(89, 'edit_subscribers', 'subscribers', '2021-05-16 07:01:24', '2021-05-16 07:01:24'),
(90, 'add_subscribers', 'subscribers', '2021-05-16 07:01:24', '2021-05-16 07:01:24'),
(91, 'delete_subscribers', 'subscribers', '2021-05-16 07:01:24', '2021-05-16 07:01:24'),
(92, 'browse_services', 'services', '2021-05-16 07:08:43', '2021-05-16 07:08:43'),
(93, 'read_services', 'services', '2021-05-16 07:08:43', '2021-05-16 07:08:43'),
(94, 'edit_services', 'services', '2021-05-16 07:08:43', '2021-05-16 07:08:43'),
(95, 'add_services', 'services', '2021-05-16 07:08:43', '2021-05-16 07:08:43'),
(96, 'delete_services', 'services', '2021-05-16 07:08:43', '2021-05-16 07:08:43'),
(97, 'browse_prices', 'prices', '2021-05-16 07:30:20', '2021-05-16 07:30:20'),
(98, 'read_prices', 'prices', '2021-05-16 07:30:20', '2021-05-16 07:30:20'),
(99, 'edit_prices', 'prices', '2021-05-16 07:30:20', '2021-05-16 07:30:20'),
(100, 'add_prices', 'prices', '2021-05-16 07:30:20', '2021-05-16 07:30:20'),
(101, 'delete_prices', 'prices', '2021-05-16 07:30:20', '2021-05-16 07:30:20'),
(102, 'browse_abouts', 'abouts', '2021-06-13 04:57:01', '2021-06-13 04:57:01'),
(103, 'read_abouts', 'abouts', '2021-06-13 04:57:01', '2021-06-13 04:57:01'),
(104, 'edit_abouts', 'abouts', '2021-06-13 04:57:01', '2021-06-13 04:57:01'),
(105, 'add_abouts', 'abouts', '2021-06-13 04:57:01', '2021-06-13 04:57:01'),
(106, 'delete_abouts', 'abouts', '2021-06-13 04:57:01', '2021-06-13 04:57:01'),
(107, 'browse_partners', 'partners', '2021-06-13 06:35:11', '2021-06-13 06:35:11'),
(108, 'read_partners', 'partners', '2021-06-13 06:35:11', '2021-06-13 06:35:11'),
(109, 'edit_partners', 'partners', '2021-06-13 06:35:11', '2021-06-13 06:35:11'),
(110, 'add_partners', 'partners', '2021-06-13 06:35:11', '2021-06-13 06:35:11'),
(111, 'delete_partners', 'partners', '2021-06-13 06:35:11', '2021-06-13 06:35:11'),
(112, 'browse_statistics', 'statistics', '2021-06-13 11:07:41', '2021-06-13 11:07:41'),
(113, 'read_statistics', 'statistics', '2021-06-13 11:07:41', '2021-06-13 11:07:41'),
(114, 'edit_statistics', 'statistics', '2021-06-13 11:07:41', '2021-06-13 11:07:41'),
(115, 'add_statistics', 'statistics', '2021-06-13 11:07:41', '2021-06-13 11:07:41'),
(116, 'delete_statistics', 'statistics', '2021-06-13 11:07:41', '2021-06-13 11:07:41'),
(117, 'browse_videos', 'videos', '2021-06-13 18:00:38', '2021-06-13 18:00:38'),
(118, 'read_videos', 'videos', '2021-06-13 18:00:38', '2021-06-13 18:00:38'),
(119, 'edit_videos', 'videos', '2021-06-13 18:00:38', '2021-06-13 18:00:38'),
(120, 'add_videos', 'videos', '2021-06-13 18:00:38', '2021-06-13 18:00:38'),
(121, 'delete_videos', 'videos', '2021-06-13 18:00:38', '2021-06-13 18:00:38'),
(122, 'browse_about_sliders', 'about_sliders', '2021-06-14 15:57:58', '2021-06-14 15:57:58'),
(123, 'read_about_sliders', 'about_sliders', '2021-06-14 15:57:58', '2021-06-14 15:57:58'),
(124, 'edit_about_sliders', 'about_sliders', '2021-06-14 15:57:58', '2021-06-14 15:57:58'),
(125, 'add_about_sliders', 'about_sliders', '2021-06-14 15:57:58', '2021-06-14 15:57:58'),
(126, 'delete_about_sliders', 'about_sliders', '2021-06-14 15:57:58', '2021-06-14 15:57:58');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(82, 3),
(83, 1),
(83, 3),
(84, 1),
(84, 3),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_hy` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_ru` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(2, 'user', 'Normal User', '2021-04-07 05:20:48', '2021-04-07 05:20:48'),
(3, 'Ընդունարան', 'Նարինե', '2021-05-08 03:34:57', '2021-05-08 03:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_hy` varchar(380) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_ru` varchar(380) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_en` varchar(380) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_hy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_ru` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `count` int(11) NOT NULL,
  `title_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullName_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullName_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullName_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profession_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profession_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profession_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_hy` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ru` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timings`
--

CREATE TABLE `timings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `day` int(11) NOT NULL,
  `start` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2021-04-07 05:20:51', '2021-04-07 05:20:51'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2021-04-07 05:20:51', '2021-04-07 05:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'gor.developer@gmail.com', 'users/default.png', NULL, '$2y$10$hFTpz38Erb843KCxxHGi8uTIFAS6DiibCV.ruSHGtqdlkoNArccBi', 'AXG0NAfMRWfr7nmtAmB0P6oV5gcKHK365OFP4DsdrdcVckc3l9dOCAURVk2z', '{\"locale\":\"en\"}', '2021-04-07 05:20:50', '2021-06-14 05:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_hy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_ru` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_sliders`
--
ALTER TABLE `about_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_posts_blog_category_id_foreign` (`blog_category_id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_service_id_foreign` (`service_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_infos`
--
ALTER TABLE `contact_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `genesis_sliders`
--
ALTER TABLE `genesis_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prices_service_id_foreign` (`service_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribers_email_unique` (`email`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timings`
--
ALTER TABLE `timings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `about_sliders`
--
ALTER TABLE `about_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_infos`
--
ALTER TABLE `contact_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `genesis_sliders`
--
ALTER TABLE `genesis_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `timings`
--
ALTER TABLE `timings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD CONSTRAINT `blog_posts_blog_category_id_foreign` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `prices`
--
ALTER TABLE `prices`
  ADD CONSTRAINT `prices_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
