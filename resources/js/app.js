import {AppToasterDuration} from "./constants/project";
import { createApp } from 'vue';
import App from './App.vue';
import { languages } from "./lang";
import store from "./store/index";
import router from "./router";
import Toaster from "@meforma/vue-toaster";
import AOS from 'aos';
import 'aos/dist/aos.css';
import VCalendar from 'v-calendar';

AOS.init();
const app = createApp(App);
app.use(languages)
app.use(store)
app.use(Toaster, {
    position: "top-right",
    duration: AppToasterDuration,
    dismissible: true
})
app.use(VCalendar)
app.use(router)
app.mount("#wrapper")

