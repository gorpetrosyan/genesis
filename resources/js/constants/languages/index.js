export const list = {
    'hy': {
        name: "Հայ",
        value: 'hy',
        href: require('../../assets/files/lang/hy.png')
    }, 'en': {
        name: 'Eng',
        value: 'en',
        href: require('../../assets/files/lang/en.png')
    }, 'ru': {
        name: 'Рус',
        value: 'ru',
        href: require('../../assets/files/lang/ru.png')
    }
}
