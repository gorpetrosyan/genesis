const mainUrl = 'http://genesis.ex';
export const PageDataLimit = 6;
export const baseUrl = `${mainUrl}/api`;
export const assetUrl = `${mainUrl}/storage/`;
export const requestTimeOut = 0;
export const AppColorFirst = "#278077";
export const AppColorSecond = "#094754";
export const AppAutoPlayInterval = 3000;
export const AppErrorTimeOut = 3500;
export const AppToasterDuration = 3000;
export const AppConfirmationDuration = 5000;
export const AppToasterClasses = {
    error : ['wk-alert'],
    warn : ['wk-warn'],
    info : ['wk-info'],
    success : ['wk-success'],
};
