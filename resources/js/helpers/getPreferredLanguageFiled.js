import getUserPreferredLanguage from "../services/language/getUserPreferredLanguage";
export default function (field){
    return `${field}_${getUserPreferredLanguage()}`
}
