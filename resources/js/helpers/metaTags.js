export default function (url, description, title, img) {
    if (removeExistingTags()) {
        document.head.prepend(createMeta(description, 'description'))
        document.head.prepend(createMeta(description, null, 'og:description'))
        document.head.prepend(createMeta(title, null, 'title'))
        document.head.prepend(createMeta(title, null, 'og:title'))
        document.head.prepend(createMeta(title, null, 'twitter:title'))
        document.head.prepend(createMeta(description, null, 'twitter:description'))
        document.head.prepend(createMeta(url, 'og:url'))
        // document.head.prepend(createMeta(img, 'og:image'))
    }
}

function createMeta(content, name, property = null) {
    const link = document.createElement('meta');
    link.content = content
    name ? link.setAttribute('name', name) : null;
    link.setAttribute('meta_element', 'meta_element');
    property ? link.setAttribute('property', property) : null;
    return link;
}

function removeExistingTags() {
    const collection = document.querySelectorAll("meta[meta_element = 'meta_element']");
    for (const elem of collection) {
        elem.remove();
    }
    return true
}
