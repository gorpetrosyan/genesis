export const RegExpList = {
    'fullName': new RegExp(/^.{3}$/),
    'email': new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/),
    'phone': new RegExp(/^(?:[+\d].*\d|\d){8,15}$/),
    'bDay': new RegExp(/^(?:[+\d].*\d|\d){8,15}$/),
    'bookingDate': new RegExp(/^(?:[+\d].*\d|\d){8,15}$/),
}
