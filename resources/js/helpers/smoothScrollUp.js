export default function (px = 100) {
    window.scroll({top: px, left: 0, behavior: 'smooth'})
}
