import { hy } from "./list/hy";
import { ru } from "./list/ru";
import { en } from "./list/en";
import { createI18n }  from "vue-i18n";
import getUserPreferredLanguage from "../services/language/getUserPreferredLanguage";
import { list } from "../constants/languages";

const messages = {
  hy, ru, en
}

// 2. Create i18n instance with options
export const languages = createI18n({
    locale: getUserPreferredLanguage(), // set locale
    fallbackLocale: list['hy'].value, // set fallback locale
    messages, // set locale messages
})
