import axios from "axios";
import { baseUrl, PageDataLimit, requestTimeOut } from "../constants/project";
import getUserPreferredLanguage from "../services/language/getUserPreferredLanguage";
export class AxiosRequest {
    // instance = {};
   constructor() {
       this.limit = PageDataLimit;
        this.requestApi = axios.create({
           baseURL: baseUrl,
           timeout: requestTimeOut,
           headers: {
               'LANGUAGE': getUserPreferredLanguage(),
           }
       })
       this.requestApi.interceptors.request.use((config) => {
           document.getElementById('loaderAttr').style.display = 'flex';
           return config;
       }, (error) => {
           document.getElementById('loaderAttr').style.display = 'none';
           return Promise.reject(error);
       })
       this.requestApi.interceptors.response.use((response) => {
           document.getElementById('loaderAttr').style.display = 'none';
           return response;
       }, (error) => {
           if(error.response.status === 404){
               location.href = "/404"
           }
           document.getElementById('loaderAttr').style.display = 'none';
           return Promise.reject(error);
       })
   }

   async get(url = null, limit = this.limit, page = 1, more = null) {
    return await this.requestApi.get(url, {
          params: {
              limit,
              page,
              more,
          }
       })
   }

    async post(url = null, data = null) {
       return await this.requestApi.post(url, data)
   }

    async put(url = null, data = null){
        return await this.requestApi.put(url, data)
   }

    async patch(url = null, data = null) {
        return await this.requestApi.patch(url, data)
   }

    async delete(url= null){
        return await this.requestApi.delete(url)
   }
}
