export default Object.freeze({
    CONTACTS: "/contacts",
    BLOG_CATEGORIES: '/blog/categories',
    CONTACT_INFO: 'contact/info',
    CONTACT_TIMING: 'contact/timing',
    TEAM: '/team',
    SUBSCRIBE: '/subscribe',
    UNSUBSCRIBE: (token) => {
       return  '/unsubscribe/' + token
    },
    CONFIRM: (token) => {
        return '/confirm/' + token
    } ,
    BLOGS: '/blogs',
    BLOG: (id) => {
        return '/blog/' + id;
    },
    BLOGS_BY_CATEGORY: (category_id) => {
        return '/blogs/category/' + category_id;
    },
    APPOINTMENT_QUESTIONS: '/appointments/questions',
    Booking: '/appointments/booking',
    SERVICES: '/services',
    LAST_SERVICE: '/last/service',
    SERVICE_BY_HASH: (hash) => {
        return 'service/' + hash
    },
    PRICE_LIST: '/price-list',
    ABOUT: '/about',
    ABOUT_SLIDER: '/about/sliders',
    PARTNERS: '/partners',
    STATISTICS: '/statistics',
    WELCOME_VIDEO: '/welcome/video',
});
