export default [
    {
        path: '/',
        name: 'home',
        component: () => import('../views/Home.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home'
                }
            ]
        },
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('../views/About.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'about'
                }
            ]
        },
    },
    {
        path: '/services',
        name: 'service',
        component: () => import('../views/Service.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'service'
                }
            ]
        },
    },
    {
        path: '/service/:hash',
        name: 'currentService',
        component: () => import('../views/CurrentService.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'service',
                    link: '/services'
                },
                {
                    name: 'blogPart.readMore'
                }
            ]
        },
    },
    // {
    //     path: '/price',
    //     name: 'price',
    //     component: () => import('../views/Price.vue'),
    //     meta: {
    //         breadcrumb: [
    //             {
    //                 name: 'home',
    //                 link: '/'
    //             },
    //             {
    //                 name: 'price'
    //             }
    //         ]
    //     },
    // },
    {
        path: '/appointment/:hash?',
        name: 'appointment',
        component: () => import('../views/Appointment.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'appointment'
                }
            ]
        },
    },
    {
        path: '/team',
        name: 'doctors',
        component: () => import('../views/Doctors.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'doctors'
                }
            ]
        },
    },
    {
        path: '/blogs',
        name: 'blog',
        component: () => import('../views/Blog.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'blog'
                }
            ]
        },
    },
    {
        path: '/blog/:id',
        name: 'currentBlog',
        component: () => import('../views/CurrentBlog.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'blog',
                    link: '/blogs'
                },
                {
                    name: 'article'
                }
            ]
        },
    },
    {
        path: '/contacts',
        name: 'contacts',
        component: () => import('../views/Contact.vue'),
        meta: {
            breadcrumb: [
                {
                    name: 'home',
                    link: '/'
                },
                {
                    name: 'contacts'
                }
            ]
        },
    },
    {
        path: '/confirm/:confirm',
        component: () => import('../views/ConfirmSubscription')
    },
    {
        path: '/unsubscribe/:unsubscribe',
        component: () => import('../views/ConfirmSubscription')
    },
    {
        path: '/:catchAll(.*)',
        name: 'Not Found',
        component: () => import('../views/NotFound'),
    }
]
