export default function () {
    let browserLanguage = window.navigator.language.slice(0,2);
    if(browserLanguage !=="en" || browserLanguage !== 'ru' || browserLanguage !== 'hy'){
        browserLanguage = 'hy'
    }
    const preferredLanguage = localStorage.getItem('language');
    return preferredLanguage ?? browserLanguage
}
