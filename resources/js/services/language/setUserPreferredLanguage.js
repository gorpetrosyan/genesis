import { list } from "../../constants/languages";
import { useI18n } from "vue-i18n";

export default function (language) {
    if(list[language]){
        localStorage.setItem('language', list[language].value);
        const { locale } = useI18n();
        locale.value = language;
    }
}
