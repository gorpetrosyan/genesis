import Loader from "../components/global/Loader";
import ContactModule from "./modules/Contacts";
export default {
    loader: Loader,
    contact: ContactModule,
}
