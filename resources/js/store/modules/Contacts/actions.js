import endpoint from "../../../request/endpoints";

export default {
    ContactStateA({commit}) {
        __request.get(endpoint.CONTACTS).then(response => {
            commit('ContactStateM', response.data)
        });
    }
}
