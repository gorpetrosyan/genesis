export default {
    changeLoaderStateAction({commit}, payload) {
        commit('changeLoaderStateMutation', payload)
    }
}
