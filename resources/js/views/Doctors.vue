<template>
    <page-dashboard :title="t('doctors')" :description="t('teamDescription')"></page-dashboard>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row justify-content-center col-mb-50 mb-0">
                    <div class="col-sm-6 col-md-4 col-xl-3" v-for="(team,index) in teams" :key="index">
                        <div class="team">
                            <div class="team-image">
                                <img :src="assetUrl + imageUrlCorrector(team['image'])" :alt="team[getPreferredLanguageFiled('fullName')]">
<!--                                <span class="info-cycle">-->
<!--                                  <i class="icon-info colorSecond"></i>-->
<!--                                </span>-->
                            </div>
<!--                            <div class="team-image-cover">-->
<!--                                <h5 id="lll">{{ team[getPreferredLanguageFiled('description')] }}</h5>-->
<!--                            </div>-->
                            <div class="team-desc">
                                <div class="team-title">
                                    <h4 class="colorSecond">{{ team[getPreferredLanguageFiled('fullName')] }}</h4>
                                    <span class="colorSecond">( {{ team[getPreferredLanguageFiled('profession')] }} )</span>
                                    <p class="colorFirst">{{ team[getPreferredLanguageFiled('description')] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <contact-doctor></contact-doctor>
        </div>
    </section>
</template>
<script>
import {defineComponent, onMounted, ref} from "vue";
import PageDashboard from "../components/global/PageDashboard";
import {useI18n} from "vue-i18n";
import ContactDoctor from "../components/doctor/ContactDoctor";
import endpoint from "../request/endpoints";
import getPreferredLanguageFiled from "../helpers/getPreferredLanguageFiled";
import imageUrlCorrector from "../helpers/imageUrlCorrector";
import {assetUrl} from "../constants/project";
import smoothScrollUp from "../helpers/smoothScrollUp";
import metaTags from "../helpers/metaTags";
import {useRoute} from "vue-router";

export default defineComponent({
    name: "Doctors",
    components: {
        ContactDoctor,
        PageDashboard
    },
    setup() {
        const {t} = useI18n();
        const teams = ref([]);
        const route = useRoute();
        const img =  ref(window.location.origin + '/images/clinic.jpg');
        function getTeamMembers(){
            __request.get(endpoint.TEAM).then(res => {
                teams.value = res.data;
            })
        }
        const fullUrl =  ref(window.location.origin + route.fullPath);
        onMounted(() => {
            getTeamMembers();
            smoothScrollUp();
            metaTags(fullUrl.value,t('doctors'),t('teamDescription'), img.value);
        })
        return {
            t,
            teams,
            getPreferredLanguageFiled,
            imageUrlCorrector,
            assetUrl,
        }
    }
})
</script>

<style scoped>
.team-image-cover {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #000000b8;
    z-index: 999;
    opacity: 0;
    transition: all 0.3s;
    cursor: pointer;
}

.team-image-cover h5 {
    color: #FFFFFF;
    text-align: center;
    line-height: 20px;
    overflow: hidden;
    margin-top: 15px;
    padding: 5px;
    word-wrap: break-word;
    font-size: 14px!important;
}

.team:hover .team-image-cover {
    display: block;
    transform: translateY(0px);
    transition-delay: 0.3s;
    opacity: 1;
}
.team:hover .info-cycle {
    display: none;
}
.info-cycle {
    position: absolute;
    top: 5px;
    left: 5px;
    display: block;
    height: 44px;
    width: 44px;
    border-radius: 50%;
    opacity: 0.9;
    text-align: center;
    border: 1px solid #094754;
}

.icon-info {
    font-size: 20px;
    line-height: 40px;
    opacity: 0.9;
    font-weight: lighter;
}
</style>
