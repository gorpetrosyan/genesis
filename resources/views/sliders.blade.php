<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slider</title>
    <link rel="stylesheet" href="{{asset('tv/css/fluid_dg.css')}}" type='text/css' media='all'>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        a {
            color: #fff;
        }
        a:hover {
            text-decoration: none;
        }
        #back_to_fluid_dg {
            background: rgba(2,2,2,.5);
            clear: both;
            display: block;
            height: 20px;
            line-height: 20px;
            padding: 20px;
            position: relative;
            z-index: 1;
        }
        .fluid_container {
            bottom: 0;
            height: 100%;
            left: 0;
            position: fixed;
            right: 0;
            top: 0;
            z-index: 0;
        }
        #fluid_dg_wrap_4 {
            bottom: 0;
            height: 100%;
            left: 0;
            margin-bottom: 0!important;
            position: fixed;
            right: 0;
            top: 0;
        }
        .fluid_dg_bar {
            z-index: 2;
        }
        .fluid_dg_prevThumbs, .fluid_dg_nextThumbs, .fluid_dg_prev, .fluid_dg_next, .fluid_dg_commands, .fluid_dg_thumbs_cont {
            background: #222;
            background: rgba(2, 2, 2, .7);
        }
        .fluid_dg_thumbs {
            margin-top: -100px;
            position: relative;
            z-index: 1;
        }
        .fluid_dg_thumbs_cont {
            border-radius: 0;
            -moz-border-radius: 0;
            -webkit-border-radius: 0;
        }
        .fluid_dg_overlayer {
            opacity: .1;
        }
        #fullScreen {
            position: fixed;
            left: 20px;
            top: 30px;
            border: 3px solid #0c5460;
            width: 20px;
            height: 20px;
            cursor: pointer;
            z-index: 999;
        }
        .fluid_dg_prev, .fluid_dg_next, .fluid_dg_commands {
            display: none;
        }
    </style>
</head>
<body>
<div class="fluid_container">
    <div id="fullScreen" onclick="openFullscreen();">
    </div>
    <div class="fluid_dg_wrap fluid_dg_emboss pattern_1 fluid_dg_white_skin" id="fluid_dg_wrap_4">
        @if(isset($sliders) && is_object($sliders))
            @foreach(json_decode($sliders['sliders'], true) as $slider)
                    <div data-src="{{defaultImage($slider)}}"></div>
            @endforeach
        @endif
    </div>
</div>
<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>

<script src="{{asset('tv/Scripts/fluid_dg.min.js')}}"></script>
<script src="{{asset('tv/Scripts/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('tv/Scripts/jquery.mobile.customized.min.js')}}"></script>
<script>
    jQuery(document).ready(function(){
        jQuery(function(){
            jQuery('#fluid_dg_wrap_4').fluid_dg({height: 'auto', loader: 'bar', pagination: false, thumbnails: true, hover: false, opacityOnGrid: false, imagePath: ''});
        }); })

    var elem = document.getElementById("fluid_dg_wrap_4");
    function openFullscreen() {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.webkitRequestFullscreen) { /* Safari */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE11 */
            elem.msRequestFullscreen();
        }
    }
</script>
</body>
</html>
