<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <link rel="manifest" href="{{asset('mix-manifest.json')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Aesthetic Medicine Center in Armenia" name="generator"/>
    <meta name="reply-to" content="{{config('mail.replay_mail')}}">
    {{--    meta tags--}}
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-0SZC6KBHNX"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-0SZC6KBHNX');
    </script>
    <meta name="subject" content="Aesthetic Medicine Center in Armenia">
    <meta name="copyright" content="{{config('app.name')}}">
    <meta name="language" content="en">
    <meta name="Classification" content="Business">
    <meta name="url" content="{{url()->current()}}">
    <meta name="identifier-URL" content="{{url()->current()}}">
    <meta name="directory" content="submission">
    <meta name="author" content="Gor Petrosyan , Գոռ Պետրոսյան">
    <meta name="reply-to" content="{{config('mail.from.address')}}">
    <meta name="owner" content="Genesis LLC">
    <meta name="category" content="Aesthetic Medicine Center">
    <meta name="coverage" content="Worldwide">
    <meta name="description" content="Genesis Aesthetic Medicine Center is a specialized medical center, where quality and efficient work are ensured thanks to highly qualified experienced specialists և innovative equipment">
    <meta name="distribution" content="Global">
    <meta name="robots" content="index, follow">
    <meta name="rating" content="General">
    <meta name="og:site_name" content="{{config('app.name')}}"/>
    <meta name="og:type" content="Medical Center"/>
    <meta property="og:title" content="Genesis Aesthetic Medicine Center" />
    <meta property="og:description" content="Genesis Aesthetic Medicine Center is a specialized medical center, where quality and efficient work are ensured thanks to highly qualified experienced specialists և innovative equipment" />
    <meta property="og:image" content="{{asset('images/clinic.jpg')}}" />
    <meta property="og:url" content="{{url()->full()}}" />
    <meta content="summary_large_image" name="twitter:card"/>
    <meta name="twitter:title" content="Genesis Aesthetic Medicine Center">
    <meta name="twitter:description" content="Genesis Aesthetic Medicine Center is a specialized medical center, where quality and efficient work are ensured thanks to highly qualified experienced specialists և innovative equipment">
    <meta name="twitter:image" content="{{asset('images/clinic.jpg')}}">
    <meta name="twitter:creator" content="@GORPETROSOV">
    <title>{{config('app.name')}}</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('logo/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('logo/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('logo/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('logo/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('logo/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('logo/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('logo/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('logo/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('logo/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('logo/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('logo/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('logo/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('logo/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('logo/manifest.json')}}">
    <meta name="msapplication-TileColor" content="{{config('app.color')}}">
    <meta name="msapplication-TileImage" content="{{asset('logo/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="{{config('app.color')}}">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '4596932810397315');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=4596932810397315&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->


    <meta name="facebook-domain-verification" content="obtz1hlnggb4na5r1cgcqnfj8w1q8n" />
</head>
<body>
<div id="wrapper" class="clearfix">

</div>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/manifest.js')}}"></script>
<script src="{{asset('js/vendor.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
