<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContactInfoController;
use App\Http\Controllers\BlogCategoryController;
use App\Http\Controllers\TimingController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\SubscriberController;
use App\Http\Controllers\BlogPostController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\StatisticController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\AboutSliderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['api'])->group(function () {
    Route::get('/contacts', [ContactController::class, 'firstItem']);
    Route::get('/contact/info', [ContactInfoController::class, 'index']);
    Route::get('/contact/timing', [TimingController::class, 'index']);
    Route::get('/blog/categories', [BlogCategoryController::class, 'index']);
    Route::get('/blogs', [BlogPostController::class, 'index']);
    Route::get('/blog/{id}', [BlogPostController::class, 'show']);
    Route::get('/blogs/category/{category_id}', [BlogPostController::class, 'indexByCategory']);
    Route::get('/team', [TeamController::class, 'index']);
    Route::post('/subscribe', [SubscriberController::class, 'subscribe']);
    Route::get('/unsubscribe/{token}', [SubscriberController::class, 'unsubscribe']);
    Route::get('/confirm/{token}', [SubscriberController::class, 'confirm']);
    Route::get('/appointments/questions', [QuestionController::class, 'index']);
    Route::post('/appointments/booking', [BookingController::class, 'create']);
    Route::get('/services', [ServiceController::class, 'index']);
    Route::get('/service/{hash}', [ServiceController::class, 'show']);
    Route::get('/last/service', [ServiceController::class, 'last']);
    Route::get('/price-list', [PriceController::class, 'index']);
    Route::get('/about', [AboutController::class, 'index']);
    Route::get('/about/sliders', [AboutSliderController::class, 'index']);
    Route::get('/partners', [PartnerController::class, 'index']);
    Route::get('/statistics', [StatisticController::class, 'index']);
    Route::get('/welcome/video', [VideoController::class, 'index']);
});
