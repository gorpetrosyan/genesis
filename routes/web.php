<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GenesisSliderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'administration/page'], function () {
    Voyager::routes();
});
Route::get('/genesis/slider/show/list',[GenesisSliderController::class,'index']);
//Route::get('/genesis/template',function (){
//    return view('mail.sendBlog');
//});
Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '.*');


